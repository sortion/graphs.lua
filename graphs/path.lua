-- graphs.lua 
-- Shortest path from a unique source to all other vertices in a weighted graph
-- Copyright (C) 2023 Samuel ORTION

-- Floyd Warshall Algorithm 
-- Input weights matrix where weights[i][j] is the weight of the edge from i to j, 0 if i=j and math.hugeinity if there is no edge from i to j
local function floyd_warshall(weights_matrix)
    local n = #weights_matrix

    local matrices = {weights_matrix}

    local function next_matrices(current_matrix, k)
        local next_matrix = {}
        for i = 1,n do
            next_matrix[i] = {}
            for j = 1,n do
                next_matrix[i][j] = math.min(current_matrix[i][j], current_matrix[i][k] + current_matrix[k][j])
            end
        end
        return next_matrix
    end
    for k = 1,n do
        local next_matrix = next_matrices(matrices[#matrices], k)
        table.insert(matrices, next_matrix)
    end
    return matrices
end

return {
    floyd_warshall = floyd_warshall,
    
}