
function print_matrix(matrix)
    for i = 1,#matrix do
        for j = 1,#matrix[i] do
            io.write(matrix[i][j] .. " ")
        end
        io.write("\n")
    end
end

return {
    print_matrix = print_matrix
}