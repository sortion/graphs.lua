package = "luagraphs"
version = "dev-1"
rockspec_format = "3.0"
source = {
   url = "git+ssh://git@framagit.org/UncleSamulus/graphs.lua.git"
}
description = {
   summary = "An attempt to implement Graphs algorithms in Lua.",
   detailed = "An attempt to implement Graphs algorithms in Lua.",
   homepage = "https://framagit.org/graphs.lua",
   license = "MIT"
}
build = {
   type = "builtin",
   modules = {
      ["graphs.graph"] = "graphs/graph.lua",
      ["graphs.path"] = "graphs/path.lua",
      ["graphs.printer"] = "graphs/printer.lua"
   }
}

test = {
   type = "command",
   script = "tests/test_graphs.lua",
}